How to generate:

```bash
python3 ./choinka.py -i <path_to_compliant_csv> -o <path_to_file_holding_drawn_data> -e <quoted_names_of_people_serving_as_elves_not_included_in_csv>
```

Minimum working example:
```bash
python3 choinka.py -i examples/the.csv -o /dev/null -e "Unsolicited elf" "Unsolicited elf 2"
```

"Elves" are generally moderators of the draw. Example .csv file format is included in examples/ directory.
CSV can be generated for instance from the XLS files. Keep in mind, that while labels aren't important, the order of fields and actual content is.

How to trigger sending:
```bash
python3 ./choinka_sender.py -i <path_to_file_holding_drawn_data> -u <gmail_user_name> -p <gmail_generated_application_password>
```

Password can be ommited - you will be asked to provide it in masked field. Currently script is hardcoded to use GMail.

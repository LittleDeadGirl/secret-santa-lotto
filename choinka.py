import argparse
import random
import copy
import math
import json
import csv
import sys
import os

def json_file(arg):
    if not os.path.exists(arg):
        raise argparse.ArgumentTypeError(f'File {arg} does not exist')
    
    try:
        with open(arg, 'r') as jin:
            out = json.loads(jin, ensure_ascii=False)
    except Exception as e:
        print(str(e))
        raise argparse.ArgumentTypeError(f'File {arg} in unexpected format')
    
    return out

def csv_choinka_file(arg):
    if not os.path.exists(arg):
        raise argparse.ArgumentTypeError(f'File {arg} does not exist')
    
    try:
        id = 0
        out = []
        reader = csv.reader(open(arg))
        first_line = next(reader)
        #if first_line[0] != 'Sygnatura czasowa':
        #    raise ValueError("Unexpected format")

        for row in reader:
            if row[0] == '':
                for cell in row:
                    if cell != '':
                        print(f'UWAGA! Pomijam niepusty wiersz: {row}')
                        break
                continue

            parsed_row = {}
            parsed_row['ts']          = row[0]
            parsed_row['name']        = row[1].strip()
            parsed_row['fb']          = row[2]
            parsed_row['foreign_ok']  = bool(row[3].lower() == 'tak' or row[3].lower() == 'yes')
            parsed_row['address']     = row[4].strip()
            parsed_row['ship_opt']    = [ opt.strip() for opt in row[5].split(',') ]
            parsed_row['inpost_no']   = row[6].strip()
            parsed_row['mobile']      = row[7].strip()
            parsed_row['email']       = row[10].strip()
            parsed_row['is_foreign']  = bool((parsed_row['mobile'].startswith('+') and 
                                               not parsed_row['mobile'].startswith('+48')) or
                                            (row[13].lower() == 'tak' or row[13].lower() == 'yes'))
            parsed_row['is_elf']      = bool(row[12].lower() == 'tak' or row[12].lower() == 'yes')
            
            out += [parsed_row]
            id += 1

    except Exception as e:
        print(str(e))
        raise argparse.ArgumentTypeError(f'File {arg} in unexpected format')
    
    return out

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', type=csv_choinka_file, help='CSV z listą choinkową',
                        required=True)
    parser.add_argument('-o', '--output', type=str, help='JSON z wynikami',
                        required=True)
    parser.add_argument('-e', '--elfs', nargs='+', default=[], required=False,
                        help='Lista elfów nie biorących udziału w wymiance')
    
    args = parser.parse_args()
    return args

def draw_mechanism(final_draw, only_elves = None):
    # First draw people
    draw = 0
    while(True):
        if draw == 0:
            print("Losuję listę wymiankową...")
        else:
            print(f"Ponownie losuję listę wymiankową... (próba nr: {draw + 1})")

        cont = False
        redo = False
        possible_santa = copy.deepcopy(final_draw)
        for i in range(len(possible_santa)):
            possible_santa[i]['id'] = i

        id_translations = []
    
        for i in range(len(final_draw)):         
            recip = random.randint(0, len(possible_santa) - 1)
            while(True):
                if final_draw[i]['name'] == possible_santa[recip]['name']:
                    if(len(possible_santa) == 1):
                        redo = True
                        break
                    else:
                        recip = random.randint(0, len(possible_santa) - 1)
                elif possible_santa[recip]['is_foreign'] and not final_draw[i]['foreign_ok']:
                    # Likely we will need to redo
                    redo = True

                    # Check if anyone has left, who can handle that
                    for j in range(i + 1, len(final_draw)):
                        if (final_draw[j]['foreign_ok'] and 
                            final_draw[j]['name'] != possible_santa[recip]['name']):

                            # Swap with someone else and cancel redo
                            redo = False
                            final_draw[i], final_draw[j] = final_draw[j], final_draw[i]
                            id_translations += [(i, j)]
                            break
                    # Either redo or accept the change if redo was cancelled
                    break
                else:
                    break
        
            if (not redo):
                final_draw[i]['target'] = possible_santa[recip]['id']
               
                # Drop the index - it is handled now
                possible_santa.pop(recip)
            else:
                cont = True
                break
        
        if not cont:
            # Apply all swaps in order, so the ids don't get mixed up :)
            for trans in id_translations:
                for drawer in final_draw:
                    if drawer['target'] == trans[0]:
                        drawer['target'] = trans[1]
                    elif drawer['target'] == trans[1]:
                        drawer['target'] = trans[0]
            break
        else:
            draw += 1
    
    # Then draw their elves
    draw = 0
    while(True):
        if draw == 0:
            print("Losuję listę powiązań elfów...")
        else:
            print(f"Ponownie losuję listę powiązań elfów... (próba nr: {draw + 1})")

        cont = False

        elf_list = {}
        for i in range(len(final_draw)):
            if 'is_elf' in final_draw[i] and final_draw[i]['is_elf']:
                elf_list[final_draw[i]['name']] = i

        if only_elves is not None:
            elves_lower = [x.lower() for x in elf_list]
            for elf in only_elves:
                if elf.lower() not in elves_lower:
                    elf_list[elf] = None

        elf_workload_exp = math.floor(float(len(final_draw)) / len(elf_list))
        elf_workload_map = {}
        for elf, _ in elf_list.items():
            elf_workload_map[elf] = 0

        for i in range(len(final_draw)):
            recip = final_draw[i]['target']
            available_elves = []
            elf_workload_avg = int(i / len(elf_list))
            for elf, idx in elf_list.items():
                if (final_draw[recip]['name'].lower() != elf.lower() and
                    final_draw[i]['name'].lower() != elf.lower() and
                    (idx is None or final_draw[idx]['target'] != i) and
                    (elf_workload_map[elf] < elf_workload_exp or 
                      (elf_workload_avg >= elf_workload_exp and
                      elf_workload_map[elf] < elf_workload_exp + 1))):
                    available_elves += [elf]

            if len(available_elves) == 0:
                # Requires full redo!
                cont = True
                break
            elif len(available_elves) == 1:
                elfip = 0
            else:
                elfip = random.randint(0, len(available_elves) - 1)
                
            final_draw[i]['elf'] = available_elves[elfip]
            elf_workload_map[final_draw[i]['elf']] += 1
        
        if not cont:
            print("=========================================================")
            print("Elfie zlecenia:")
            for elf in elf_list:
                print(f"  {elf} ma {elf_workload_map[elf]} zleceń")
            print("=========================================================")

            break
        else:
            draw += 1
    
    return final_draw

if __name__ == "__main__":
    args = parse_args()

    # Select at random without repetitions
    final_draw = draw_mechanism(args.input, args.elfs)

    # Finalize and print data overview
    for i in range(len(final_draw)):
        final_draw[i]['id'] = i
        receiver_ref = final_draw[i]['target']
        print(f"{final_draw[i]['name']} jest Chustołajem {final_draw[receiver_ref]['name']}"
               f" (elf: {final_draw[i]['elf']})")

    with open(args.output, 'w', encoding='utf8') as jout:
        json.dump(final_draw, jout, ensure_ascii=False, indent=2)

    # Final verification
    verification_failed = False
    drawn_list = []
    for drawer in final_draw:
        if drawer['target'] is None:
            verification_failed = True
            print(f"{drawer['name']} nie wysyła nikomu prezentu!")

        if drawer['target'] in drawn_list:
            verification_failed = True
            print(f"{final_draw[drawer['target']]['name']} otrzyma więcej niż jeden prezent!")
        
        if drawer['mobile'] == '':
            verification_failed = True
            print(f"{drawer['name']} nie ma numeru telefonu!")

        if drawer['email'] == '':
            verification_failed = True
            print(f"{drawer['name']} nie ma adresu e-mail!")

        if drawer['elf'] == '':
            verification_failed = True
            print(f"{drawer['name']} nie ma przypisanego elfa!")
        
        if final_draw[drawer['target']]['is_foreign'] and not drawer['foreign_ok']:
            verification_failed = True
            print(f"{drawer['name']} nie chce wysyłki za granicę, "
                  f"a jest Chustołajem dla {final_draw[drawer['target']]['name']}!")

    if verification_failed:
        print("Coś poszło no-yes, kończymy! (Plik wyjściowy jest zachowany)")
        sys.exit(2)
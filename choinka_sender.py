from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import argparse
import smtplib
import getpass
import time
import json
import sys
import os

# Constants
BUDGET = 100
SPAM_DET_PREV_SEC = 2

# Globals
processed_file = None

def json_file(arg):
    if not os.path.exists(arg):
        raise argparse.ArgumentTypeError(f'File {arg} does not exist')
    
    try:
        with open(arg, 'r', encoding='utf8') as jin:
            out = json.load(jin)
    except Exception as e:
        print(str(e))
        raise argparse.ArgumentTypeError(f'File {arg} in unexpected format')

    global processed_file
    processed_file = arg
    
    return out

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', type=json_file, help='JSON z wylosowaną grupą',
                        required=True)
    parser.add_argument('-u', '--user', type=str, help='Użytkownik do wysłania e-maili',
                        required=True)
    parser.add_argument('-p', '--password', type=str,
                        help='Hasło użytkownika do wysłania e-maili (może być podane później)')
    
    args = parser.parse_args()
    return args

# Do what you need for you to have the message you want
def format_mail(sender, recipient, address, shipping_list, elf):
    return \
f"""Ho ho ho! {sender},
    
W tym roku będziesz Chustołajem dla {recipient}!

Dane wysyłkowe: {address}

Preferowana wysłka: {', '.join(shipping_list)}
    
Pamiętaj proszę, że budżet to {BUDGET}zł plus wszystko co uda się zdobyć w ramach barteru/przeszukiwania domowych zasobów 
(oczywiście pozostając w zgodzie z opisem obdarowanej osoby). 

Osoba odpowiedzialna za poprowadzenie Cię przez cudowny czas przygotowywania prezentu to: {elf}. 
Zgłaszaj się do niej ze wszystkimi pytaniami lub kiedy będziesz potrzebować rady. 
Pisz również, jeśli zabraknie Ci pomysłów, albo po prostu masz ochotę z kimś je skonsultować.

Elf czeka na Twój plan prezentu - wyślij go proszę we wrześniu.

Przeczytaj dokładnie opis osoby, która została dla Ciebie wylosowana i przygotuj dla niej najcudowniejszy prezent na świecie.
Taki, jaki sam* chciał*byś znaleźć pod choinką. Postaraj się dowiedzieć jak najwięcej o wylosowanej osobie, również przeszukując 
jej media społecznościowe. W tej zabawie chodzi przede wszystkim o przyjaźń i sprawienie drugiej osobie ogromnej radości.

Cudownej zabawy!
"""

def send_out_everything(final_draw, user, password):
    for i in range(len(final_draw)):
        # Omit the recipients already marked as processed earlier
        if ('was_sent' in final_draw[i] and final_draw[i]['was_sent']):
            continue

        print(f"Wysyłam notyfikację do {final_draw[i]['name']}...")
        receiver_ref = final_draw[i]['target']
    
        # the message which will be sent in the email
        mail_content = format_mail(final_draw[i]['name'],
                                   final_draw[receiver_ref]['name'],
                                   final_draw[receiver_ref]['address'],
                                   final_draw[receiver_ref]['ship_opt'],
                                   final_draw[i]['elf'])
    
        # sets the email address the email will be sent to
        receiver_address = final_draw[i]['email']
    
        # sets up the MIME
        message = MIMEMultipart()
        message['From'] = f'{user}@gmail.com' # your email address
        message['To'] = receiver_address # Secret Santa's email address
        message['Subject'] = 'Chustowa Choinka 2023' # subject of the 
    
        # sets the body of the mail
        message.attach(MIMEText(mail_content, 'plain'))
    
        # creates the SMTP session for sending the mail
        session = smtplib.SMTP('smtp.gmail.com', 587)
        session.connect("smtp.gmail.com", 587)
        session.ehlo()
        session.starttls()
        session.login(user, password)
        text = message.as_string()
        session.sendmail(user, receiver_address, text)
        session.quit()

        # To prevent treating this as SPAM do a SPAM_DET_PREV_SEC second 
        # sleep between sends
        time.sleep(SPAM_DET_PREV_SEC)

        # Save the state
        final_draw[i]['was_sent'] = True

if __name__ == "__main__":
    args = parse_args()

    # Fill up the gaps
    if args.user is not None and args.password is None:
        password = getpass.getpass(prompt=f'Podaj hasło użytkownika {args.user}: ')
    else:
        password = args.password

    # Get the input json
    final_draw = args.input

    # Final verification
    verification_failed = False
    drawn_list = []
    for drawer in final_draw:
        if drawer['target'] is None:
            verification_failed = True
            print(f"{drawer['name']} nie wysyła nikomu prezentu!")

        if drawer['target'] in drawn_list:
            verification_failed = True
            print(f"{final_draw[drawer['target']]['name']} otrzyma więcej niż jeden prezent!")

        if drawer['email'] == '':
            verification_failed = True
            print(f"{drawer['name']} nie ma adresu e-mail!")

        if drawer['elf'] == '':
            verification_failed = True
            print(f"{drawer['name']} nie ma przypisanego elfa!")
        
        if final_draw[drawer['target']]['is_foreign'] and not drawer['foreign_ok']:
            verification_failed = True
            print(f"{drawer['name']} nie nie chce wysyłki za granicę, "
                  f"a jest Chustołajem dla {final_draw[drawer['target']]['name']}!")

    if verification_failed:
        print("Coś poszło no-yes, kończymy!")
        sys.exit(2)

    # Send data if user and password is set
    print("Wysyłam notyfikacje (już za późno na poprawki!)")
    try:
        send_out_everything(final_draw, args.user, password)
        print("To już. Darki dla autorów mile widziane :)")
    except Exception as e:
        print('Błąd podczas wysyłki - stan wysyłki zostanie zachowany...')
        print(str(e))
    finally:
        with open(processed_file, 'w', encoding='utf8') as jout:
            json.dump(final_draw, jout, ensure_ascii=False, indent=2)
